package com.example.cocktailappxml.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.cocktailappxml.adapter.DrinkAdapter
import com.example.cocktailappxml.databinding.FragmentDashboardBinding
import com.example.cocktailappxml.util.Resource
import com.example.cocktailappxml.viewmodel.DrinkViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Dashboard fragment.
 *
 * @constructor Create empty Dashboard fragment
 */
@AndroidEntryPoint
class DashboardFragment : Fragment() {

    @Suppress("VariableNaming", "ObjectPropertyNaming")
    var _binding: FragmentDashboardBinding? = null
    val binding: FragmentDashboardBinding get() = _binding!!
    val viewModel by viewModels<DrinkViewModel>()
    val drinkAdapter by lazy { DrinkAdapter(::navigateToCategory) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentDashboardBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initViews() = with(binding) {
        viewModel.drinkList.observe(
            viewLifecycleOwner,
            Observer { drinkList ->
                when (drinkList) {
                    is Resource.Error -> TODO()
                    Resource.Loading -> TODO()
                    is Resource.Success ->
                        rvView.adapter = drinkAdapter.apply { addDrink(drinkList.data) }
                }
            }
        )
    }

    /**
     * Navigate to category.
     *
     * @param drinkName
     */
    fun navigateToCategory(drinkName: String) {
        val action =
            DashboardFragmentDirections.actionDashboardFragmentToCategoryFragment(drinkName)
        findNavController().navigate(action)
    }
}
