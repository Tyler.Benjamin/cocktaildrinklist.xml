package com.example.cocktailappxml.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.cocktailappxml.R
import com.example.cocktailappxml.adapter.RecipeAdapter
import com.example.cocktailappxml.databinding.FragmentRecipeBinding
import com.example.cocktailappxml.util.Resource
import com.example.cocktailappxml.viewmodel.RecipeViewModel
import kotlinx.coroutines.launch

/**
 * Recipe fragment.
 *
 * @constructor Create empty Recipe fragment
 */
class RecipeFragment : Fragment() {

    private var _binding: FragmentRecipeBinding? = null
    val binding: FragmentRecipeBinding get() = _binding!!
    val recipeVModel by activityViewModels<RecipeViewModel>()
    val args: RecipeFragmentArgs by navArgs()
    val recipeAadapter by lazy { RecipeAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentRecipeBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(
            view,
            savedInstanceState
        )
        initViews()
        binding.homeButton.setOnClickListener {
            navigateHome()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Init views is called and used similar to how an init block will work. this is called in onViewCreated.
     *
     */
    private fun initViews() =
        lifecycleScope.launch {
            recipeVModel.getRecipeList(args.drinkName)
            recipeVModel.recipeList.observe(
                viewLifecycleOwner,
                Observer { recipeList ->
                    when (recipeList) {
                        is Resource.Error -> TODO()
                        Resource.Loading -> TODO()
                        is Resource.Success ->
                            binding.rvViewRecipe.adapter = recipeAadapter.apply {
                                addRecipes(recipeList.data)
                            }
                    }
                }
            )
        }

    /**
     * Navigate home function.
     *
     */
    fun navigateHome() {
        findNavController().navigate(R.id.dashboardFragment)
    }
}
