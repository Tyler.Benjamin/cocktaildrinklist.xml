package com.example.cocktailappxml.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.cocktailappxml.adapter.CategoryAdapter
import com.example.cocktailappxml.databinding.FragmentCategoryBinding
import com.example.cocktailappxml.util.Resource
import com.example.cocktailappxml.viewmodel.CategoryViewModel
import kotlinx.coroutines.launch

/**
 * Category fragment.
 *
 * @constructor Create empty Category fragment
 */
class CategoryFragment : Fragment() {

    @Suppress("VariableNaming", "ObjectPropertyNaming")
    var _binding: FragmentCategoryBinding? = null
    val binding: FragmentCategoryBinding get() = _binding!!
    val viewModel by activityViewModels<CategoryViewModel>()
    val args: CategoryFragmentArgs by navArgs()
    val categoryAdapter by lazy { CategoryAdapter(::navigateToRecipe) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        FragmentCategoryBinding.inflate(inflater, container, false).also {
            _binding = it
        }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Init views.
     *
     */
    fun initViews() = with(binding) {
        lifecycleScope.launch {
            viewModel.getCategoryList(args.strCategory)
            viewModel.categoryList.observe(
                viewLifecycleOwner,
                Observer { categoryList ->
                    when (categoryList) {
                        is Resource.Error -> TODO()
                        Resource.Loading -> TODO()
                        is Resource.Success ->
                            rvViewCategory.adapter =
                                categoryAdapter.apply { addCategory(categoryList.data) }
                    }
                }
            )
        }
    }

    /**
     * Navigate to recipe.
     *
     * @param drinkName
     */
    fun navigateToRecipe(drinkName: String) {
        val action = CategoryFragmentDirections.actionCategoryFragmentToRecipeFragment(drinkName)
        findNavController().navigate(action)
    }
}
