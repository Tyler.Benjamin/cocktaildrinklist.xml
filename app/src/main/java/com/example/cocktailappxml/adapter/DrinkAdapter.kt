package com.example.cocktailappxml.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.cocktailappxml.databinding.ItemListBinding
import com.example.cocktailappxml.model.local.entity.Drink
import com.example.cocktailappxml.util.Constants.DRINK_IMAGE

/**
 * Drink adapter for displaying initial list.
 *
 * @property navigate
 * @constructor Create empty Drink adapter
 */
class DrinkAdapter(
    val navigate: (strCategory: String) -> Unit
) : RecyclerView.Adapter<DrinkAdapter.DrinkViewHolder>() {
    private var drinkList: List<Drink> = mutableListOf()

    /**
     * Drink view holder.
     *
     * @property binding
     * @constructor Create empty Drink view holder
     */
    inner class DrinkViewHolder(
        private val binding: ItemListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        /**
         * Apply drink for the binding for each item view on the list.
         *
         * @param drink
         */
        fun applyDrink(drink: Drink) = with(binding) {
            tvDrink.text = drink.strCategory
            imageViewDrink.load(DRINK_IMAGE)

            root.setOnClickListener {
                println("ROOT HAS BEEN CLICKED")
                navigate(drink.strCategory)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkViewHolder {
        val itemBinding = ItemListBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return DrinkViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: DrinkViewHolder, position: Int) {
        val item = drinkList[position]
        holder.applyDrink(item)
    }

    override fun getItemCount(): Int = drinkList.size

    /**
     * Add drink - public function called in the fragment that pushes list retrieved from fragment to the adapter.
     *
     * @param drinkList
     */
    fun addDrink(drinkList: List<Drink>) {
        this.drinkList = drinkList
    }
//    fun navigateToCategory(strCategory: String){
//        val action = DashboardFragmentDirections.actionDashboardFragmentToCategoryFragment(strCategory)
//        findNavController(fragment = DashboardFragment()).navigate(strCategory)
//    }
}
