package com.example.cocktailappxml.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.cocktailappxml.databinding.ItemListBinding
import com.example.cocktailappxml.model.local.entity.CategoryDrink

/**
 * Category adapter dor displaying selected Category drinks.
 *
 * @property navigate
 * @constructor Create empty Category adapter
 */
class CategoryAdapter(
    val navigate: (strDrink: String) -> Unit
) : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {
    private var categoryList: List<CategoryDrink> = mutableListOf()

    /**
     * Category view holder for displaying view.
     *
     * @property binding
     * @constructor Create empty Category view holder
     */
    inner class CategoryViewHolder(
        private var binding: ItemListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        /**
         * Apply category.
         *
         * @param category
         */
        fun applyCategory(category: CategoryDrink) {
            binding.tvDrink.text = category.strDrink
            binding.imageViewDrink.load(category.strDrinkThumb)
            binding.root.setOnClickListener {
                navigate(category.strDrink)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val itemListBinding = ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CategoryViewHolder(itemListBinding)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val drink = categoryList[position]
        holder.applyCategory(drink)
    }

    override fun getItemCount(): Int = categoryList.size

    /**
     * Add category function used to pass list from fragment to the adapter.
     *
     * @param categoryList
     */
    fun addCategory(categoryList: List<CategoryDrink>) {
        this.categoryList = categoryList
    }
}
