package com.example.cocktailappxml.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.cocktailappxml.databinding.ItemRecipeBinding
import com.example.cocktailappxml.model.local.entity.RecipeDrink

/**
 * Recipe adapter fo displaying list of recipes.
 *
 * @constructor Create empty Recipe adapter
 */
class RecipeAdapter : RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder>() {

    var recipeList: List<RecipeDrink> = mutableListOf()

    /**
     * Recipe view holder.
     *
     * @property binding
     * @constructor Create empty Recipe view holder
     */
    inner class RecipeViewHolder(
        private val binding: ItemRecipeBinding

    ) : RecyclerView.ViewHolder(binding.root) {

        /**
         * Apply recipe - used for binding the data from api to XML.
         *
         * @param recipe
         */
        @Suppress("indent")
        fun applyRecipe(recipe: RecipeDrink) = with(binding) {
            @Suppress("indent")
            val ingredientText: String =
                """
                    List of ingredients are: ${recipe.strIngredient1}," +
                    "${recipe.strIngredient2}, ${recipe.strIngredient3}, ${recipe.strIngredient4},
                    ${recipe.strIngredient5}, ${recipe.strIngredient6}, ${recipe.strIngredient7}"""
                    .trimMargin()

            @Suppress("indent")
            val measurementText =
                """
                List of Measurements are: ${recipe.strMeasure1}, ${recipe.strMeasure2},
                ${recipe.strMeasure3},${recipe.strMeasure4},${recipe.strMeasure5},
                ${recipe.strMeasure6},${recipe.strMeasure7},${recipe.strMeasure8}
                """.trimIndent()
            imageViewDrink.load(recipe.strDrinkThumb)
            tvStrDrink.text = recipe.strDrink
            strCategory.text = recipe.strCategory
            strAlcoholic.text = recipe.strAlcoholic
            strIngredient.text = ingredientText
            strMeasurement.text = measurementText
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
        val recipeBinding = ItemRecipeBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return RecipeViewHolder(recipeBinding)
    }

    override fun onBindViewHolder(
        holder: RecipeViewHolder,
        position: Int
    ) {
        val recipe = recipeList[position]
        holder.applyRecipe(recipe)
    }

    override fun getItemCount(): Int = recipeList.size

    /**
     * Add recipes - public function used to pass dlist data from fragment to the adapter.
     *
     * @param recipeList
     */
    fun addRecipes(recipeList: List<RecipeDrink>) {
        this.recipeList = recipeList
    }
}
