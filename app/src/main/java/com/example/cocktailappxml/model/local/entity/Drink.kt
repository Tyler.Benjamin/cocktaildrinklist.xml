package com.example.cocktailappxml.model.local.entity

/**
 * Drink.
 *
 * @property strCategory
 * @constructor Create empty Drink
 */
data class Drink(
    val strCategory: String
)
