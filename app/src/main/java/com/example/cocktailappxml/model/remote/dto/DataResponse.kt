package com.example.cocktailappxml.model.remote.dto

/**
 * Data response for initial list.
 *
 * @property drinks
 * @constructor Create empty Data response
 */
data class DataResponse(
    val drinks: List<DrinkDTO>
)
