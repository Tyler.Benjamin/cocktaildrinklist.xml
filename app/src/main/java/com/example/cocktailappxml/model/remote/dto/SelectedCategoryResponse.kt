package com.example.cocktailappxml.model.remote.dto

/**
 * Selected category response.
 *
 * @property drinks
 * @constructor Create empty Selected category response
 */
data class SelectedCategoryResponse(
    val drinks: List<CategoryDrinkDTO>
)
