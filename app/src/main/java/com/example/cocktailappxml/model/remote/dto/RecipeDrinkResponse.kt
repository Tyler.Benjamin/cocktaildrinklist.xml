package com.example.cocktailappxml.model.remote.dto

/**
 * Recipe drink response from hitting RECIPE ENd Point.
 *
 * @property drinks
 * @constructor Create empty Recipe drink response
 */
data class RecipeDrinkResponse(
    val drinks: List<RecipeDrinkDTO>
)
