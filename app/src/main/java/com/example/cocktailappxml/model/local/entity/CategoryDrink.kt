package com.example.cocktailappxml.model.local.entity

/**
 * Category drink.
 *
 * @property strDrink
 * @property strDrinkThumb
 * @property idDrink
 * @constructor Create empty Category drink
 */
data class CategoryDrink(
    val strDrink: String,
    val strDrinkThumb: String,
    val idDrink: String
)
