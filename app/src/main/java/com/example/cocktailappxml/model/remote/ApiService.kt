package com.example.cocktailappxml.model.remote

import com.example.cocktailappxml.model.remote.dto.DataResponse
import com.example.cocktailappxml.model.remote.dto.RecipeDrinkResponse
import com.example.cocktailappxml.model.remote.dto.SelectedCategoryResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Api service.
 *
 * @constructor Create empty Api service
 */
interface ApiService {

    /**
     * Get drink list.
     *
     * @param list
     * @return
     */
    @GET(LIST_ENDPOINT)
    suspend fun getDrinkList(@Query("c") list: String = "list"): Response<DataResponse>

    /**
     * Get selected category list.
     *
     * @param filter
     * @return
     */
    @GET(CATEGORY_ENDPOINT)
    suspend fun getSelectedCategoryList(@Query("c") filter: String): Response<SelectedCategoryResponse>

    /**
     * Get selected drink recipe.
     *
     * @param search
     * @return
     */
    @GET(RECIPE_ENDPOINT)
    suspend fun getSelectedDrinkRecipe(@Query("s") search: String): Response<RecipeDrinkResponse>

    /**
     * Companion object fof holding end point references.
     *
     * @constructor Create empty Companion
     */
    companion object {
        const val LIST_ENDPOINT = "list.php"
        const val CATEGORY_ENDPOINT = "filter.php"
        const val RECIPE_ENDPOINT = "search.php"
    }
}
