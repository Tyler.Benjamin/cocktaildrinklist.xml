package com.example.cocktailappxml.model.remote.dto

/**
 * Drink d t o.
 *
 * @property strCategory
 * @constructor Create empty Drink d t o
 */
data class DrinkDTO(
    val strCategory: String
)
