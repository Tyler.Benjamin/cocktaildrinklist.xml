package com.example.cocktailappxml.model.remote.dto

/**
 * Category drink d t o.
 *
 * @property strDrink
 * @property strDrinkThumb
 * @property idDrink
 * @constructor Create empty Category drink d t o
 */
data class CategoryDrinkDTO(
    val strDrink: String,
    val strDrinkThumb: String,
    val idDrink: String
)
