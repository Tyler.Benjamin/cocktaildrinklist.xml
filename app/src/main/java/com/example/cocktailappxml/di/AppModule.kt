package com.example.cocktailappxml.di

import com.example.cocktailappxml.data.RepositoryImpl
import com.example.cocktailappxml.model.remote.ApiService
import com.example.cocktailappxml.repository.Repository
import com.example.cocktailappxml.util.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * App module for DI purposes.
 *
 * @constructor Create empty App module
 */
@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun providesApiService(): ApiService {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    @Provides
    @Singleton
    fun providesRepositoryImp(apiService: ApiService): Repository = RepositoryImpl(apiService)
}
