package com.example.cocktailappxml.util.mapper

import com.example.cocktailappxml.model.local.entity.CategoryDrink
import com.example.cocktailappxml.model.remote.dto.CategoryDrinkDTO

/**
 * Category drink mapper.
 *
 * @constructor Create empty Category drink mapper
 */
class CategoryDrinkMapper : Mapper<CategoryDrinkDTO, CategoryDrink> {
    override fun invoke(dto: CategoryDrinkDTO): CategoryDrink = with(dto) {
        return CategoryDrink(strDrink ?: "", strDrinkThumb ?: "", idDrink ?: "")
    }
}
