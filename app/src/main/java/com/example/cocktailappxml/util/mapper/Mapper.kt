package com.example.cocktailappxml.util.mapper

/**
 * Mapper.
 *
 * @param DTO
 * @param ENTITY
 * @constructor Create empty Mapper
 */
interface Mapper<in DTO, out ENTITY> {
    /**
     * Invoke conversion.
     *
     * @param dto
     * @return
     */
    operator fun invoke(dto: DTO): ENTITY
}
