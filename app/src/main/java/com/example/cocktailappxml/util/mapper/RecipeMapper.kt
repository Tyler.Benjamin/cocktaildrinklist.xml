package com.example.cocktailappxml.util.mapper

import com.example.cocktailappxml.model.local.entity.RecipeDrink
import com.example.cocktailappxml.model.remote.dto.RecipeDrinkDTO

/**
 * Recipe drink mapper.
 *
 * @constructor Create empty Recipe mapper
 */
class RecipeMapper : Mapper<RecipeDrinkDTO, RecipeDrink> {
    override fun invoke(dto: RecipeDrinkDTO): RecipeDrink = with(dto) {
        return RecipeDrink(
            dateModified ?: "",
            idDrink ?: "",
            strAlcoholic ?: "",
            strCategory ?: "",
            strCreativeCommonsConfirmed,
            strDrink ?: "",
            strDrinkAlternate ?: "",
            strDrinkThumb ?: "",
            strGlass ?: "",
            strIBA ?: "",
            strImageAttribution ?: "",
            strImageSource ?: "",
            strIngredient1 ?: "",
            strIngredient10 ?: "",
            strIngredient11 ?: "",
            strIngredient12 ?: "",
            strIngredient13 ?: "",
            strIngredient14 ?: "",
            strIngredient15 ?: "",
            strIngredient2 ?: "",
            strIngredient3 ?: "",
            strIngredient4 ?: "",
            strIngredient5 ?: "",
            strIngredient6 ?: "",
            strIngredient7 ?: "",
            strIngredient8 ?: "",
            strIngredient9 ?: "",
            strInstructions ?: "",
            strInstructionsDE ?: "",
            strInstructionsES ?: "",
            strInstructionsFR ?: "",
            strInstructionsIT ?: "",
            strInstructionszhHANS ?: "",
            strInstructionszhHANT ?: "",
            strMeasure1 ?: "",
            strMeasure10 ?: "",
            strMeasure11 ?: "",
            strMeasure12 ?: "",
            strMeasure13 ?: "",
            strMeasure14 ?: "",
            strMeasure15 ?: "",
            strMeasure2 ?: "",
            strMeasure3 ?: "",
            strMeasure4 ?: "",
            strMeasure5 ?: "",
            strMeasure6 ?: "",
            strMeasure7 ?: "",
            strMeasure8 ?: "",
            strMeasure9 ?: "",
            strTags ?: "",
            strVideo ?: ""
        )
    }
}
