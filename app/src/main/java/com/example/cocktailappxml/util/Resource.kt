package com.example.cocktailappxml.util

/**
 * Resource.
 *
 * @param T
 * @constructor Create empty Resource
 */
sealed class Resource<out T : Any> {
    /**
     * Success.
     *
     * @param T
     * @property data
     * @constructor Create empty Success
     */
    data class Success<out T : Any>(val data: T) : Resource<T>()

    /**
     * Error.
     *
     * @property exception
     * @constructor Create empty Error
     */
    data class Error(val exception: String) : Resource<Nothing>()

    /**
     * Loading.
     *
     * @constructor Create empty Loading
     */
    object Loading : Resource<Nothing>()
}
