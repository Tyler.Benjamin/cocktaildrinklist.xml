package com.example.cocktailappxml.util

/**
 * Constant values.
 *
 * @constructor Create empty Constants
 */
object Constants {
    const val BASE_URL = " https://www.thecocktaildb.com/api/json/v1/1/"
    const val DRINK_IMAGE =
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShd6NH9a_IdYOQm4jOeAyCyEEV1Qm_9sX1sQ&usqp=CAU"
}
