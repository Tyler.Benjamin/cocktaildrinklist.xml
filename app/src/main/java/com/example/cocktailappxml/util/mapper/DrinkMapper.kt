package com.example.cocktailappxml.util.mapper

import com.example.cocktailappxml.model.local.entity.Drink
import com.example.cocktailappxml.model.remote.dto.DrinkDTO

/**
 * Drink mapper.
 *
 * @constructor Create empty Drink mapper
 */
class DrinkMapper : Mapper<DrinkDTO, Drink> {
    override fun invoke(dto: DrinkDTO): Drink {
        return with(dto) {
            Drink(strCategory = strCategory ?: "")
        }
    }
}
