package com.example.cocktailappxml.repository

import com.example.cocktailappxml.model.local.entity.CategoryDrink
import com.example.cocktailappxml.model.local.entity.Drink
import com.example.cocktailappxml.model.local.entity.RecipeDrink
import com.example.cocktailappxml.util.Resource

/**
 * Repository.
 *
 * @constructor Create empty Repository
 */
interface Repository {
    /**
     * Get drink list.
     *
     * @param categoryList
     * @return
     */
    suspend fun getDrinkList(categoryList: String = "list"): Resource<List<Drink>>

    /**
     * Get selected category list.
     *
     * @param filter
     * @return
     */
    suspend fun getSelectedCategoryList(filter: String): Resource<List<CategoryDrink>>

    /**
     * Get selected drink recipe.
     *
     * @param search
     * @return
     */
    suspend fun getSelectedDrinkRecipe(search: String): Resource<List<RecipeDrink>>
}
