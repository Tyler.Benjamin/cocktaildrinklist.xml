package com.example.cocktailappxml.data

import com.example.cocktailappxml.model.local.entity.CategoryDrink
import com.example.cocktailappxml.model.local.entity.Drink
import com.example.cocktailappxml.model.local.entity.RecipeDrink
import com.example.cocktailappxml.model.remote.ApiService
import com.example.cocktailappxml.repository.Repository
import com.example.cocktailappxml.util.Resource
import com.example.cocktailappxml.util.mapper.CategoryDrinkMapper
import com.example.cocktailappxml.util.mapper.DrinkMapper
import com.example.cocktailappxml.util.mapper.RecipeMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Repository impl.
 *
 * @property apiService
 * @constructor Create empty Repository impl
 */
class RepositoryImpl @Inject constructor(private val apiService: ApiService) : Repository {
    private val mapper = DrinkMapper()
    private val categoryMapper = CategoryDrinkMapper()
    private val recipeMapper = RecipeMapper()

    override suspend fun getDrinkList(categoryList: String): Resource<List<Drink>> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val res = apiService.getDrinkList("list")
                if (res.isSuccessful && res.body() != null) {
                    Resource.Success(res.body()!!.drinks.map { mapper(it) })
                } else {
                    Resource.Error(res.message())
                }
            } catch (e: IllegalArgumentException) {
                Resource.Error(e.message.toString())
            }
        }

    override suspend fun getSelectedCategoryList(filter: String): Resource<List<CategoryDrink>> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val res = apiService.getSelectedCategoryList(filter)
                if (res.isSuccessful && res.body() != null) {
                    Resource.Success(res.body()!!.drinks.map { categoryMapper(it) })
                } else {
                    Resource.Error(res.message())
                }
            } catch (e: IllegalArgumentException) {
                Resource.Error(e.message.toString())
            }
        }

    override suspend fun getSelectedDrinkRecipe(search: String): Resource<List<RecipeDrink>> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val res = apiService.getSelectedDrinkRecipe(search)
                if (res.isSuccessful && res.body() != null) {
                    Resource.Success(res.body()!!.drinks.map { recipeMapper(it) })
                } else {
                    Resource.Error(res.message())
                }
            } catch (e: IllegalArgumentException) {
                Resource.Error(e.message.toString())
            }
        }
}
