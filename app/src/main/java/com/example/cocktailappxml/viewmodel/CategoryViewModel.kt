package com.example.cocktailappxml.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailappxml.data.RepositoryImpl
import com.example.cocktailappxml.model.local.entity.CategoryDrink
import com.example.cocktailappxml.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Category view model.
 *
 * @property repo
 * @constructor Create empty Category view model
 */
@HiltViewModel
class CategoryViewModel @Inject constructor(private val repo: RepositoryImpl) : ViewModel() {
    private val _categoryList: MutableLiveData<Resource<List<CategoryDrink>>> = MutableLiveData()
    val categoryList: LiveData<Resource<List<CategoryDrink>>> = _categoryList

    /**
     * Get category list.
     *
     * @param filter
     */
    fun getCategoryList(filter: String) {
        viewModelScope.launch {
            _categoryList.value = repo.getSelectedCategoryList(filter)
        }
    }
}
