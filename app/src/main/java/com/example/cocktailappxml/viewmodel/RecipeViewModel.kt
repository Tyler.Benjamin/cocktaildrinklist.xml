package com.example.cocktailappxml.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailappxml.data.RepositoryImpl
import com.example.cocktailappxml.model.local.entity.RecipeDrink
import com.example.cocktailappxml.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Recipe view model.
 *
 * @property repo
 * @constructor Create empty Recipe view model
 */
@HiltViewModel
class RecipeViewModel @Inject constructor(private val repo: RepositoryImpl) : ViewModel() {

    private var _recipeList: MutableLiveData<Resource<List<RecipeDrink>>> = MutableLiveData()
    val recipeList: LiveData<Resource<List<RecipeDrink>>> = _recipeList

    /**
     * Get recipe list from API.
     *
     * @param strDrink
     */
    fun getRecipeList(strDrink: String) {
        viewModelScope.launch {
            _recipeList.value = repo.getSelectedDrinkRecipe(strDrink)
        }
    }
}
