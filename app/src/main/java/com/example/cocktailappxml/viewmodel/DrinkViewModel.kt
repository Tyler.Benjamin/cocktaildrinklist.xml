package com.example.cocktailappxml.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailappxml.data.RepositoryImpl
import com.example.cocktailappxml.model.local.entity.Drink
import com.example.cocktailappxml.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Drink view model.
 *
 * @property repo
 * @constructor Create empty Drink view model
 */
@HiltViewModel
class DrinkViewModel @Inject constructor(private val repo: RepositoryImpl) : ViewModel() {
    private val _drinkList: MutableLiveData<Resource<List<Drink>>> = MutableLiveData()
    val drinkList: LiveData<Resource<List<Drink>>> = _drinkList

    init {
        getDrinkList()
    }

    /**
     * Get drink list.
     *
     */
    fun getDrinkList() {
        viewModelScope.launch {
            _drinkList.value = repo.getDrinkList()
        }
    }
}
