package com.example.cocktailappxml.viewmodel

import com.example.cocktailappxml.data.RepositoryImpl
import com.example.cocktailappxml.model.local.entity.Drink
import com.example.cocktailappxml.util.Resource
import com.util.CoroutinesTestExtension
import com.util.InstantTaskExecutorExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.RegisterExtension

@ExtendWith(InstantTaskExecutorExtension::class)
internal class DrinkViewModelTest {
    @OptIn(ExperimentalCoroutinesApi::class)
    @RegisterExtension
    private val coroutinesTestExtension = CoroutinesTestExtension()
    private val repo = mockk<RepositoryImpl>()

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    @DisplayName("Test View Model")
    fun testViewModel1() = runTest(coroutinesTestExtension.dispatcher) {
        // given
        val list = listOf(Drink(strCategory = ""))
        val fakeResponse = Resource.Success(list)

        coEvery { repo.getDrinkList() } coAnswers { fakeResponse }

        val vm = DrinkViewModel(repo)
        // then
        // assertTrue(vm.drinkList is MutableLiveData)
        val result = vm.drinkList.value

        assertEquals(
            result,
            fakeResponse
        )
    }
}
