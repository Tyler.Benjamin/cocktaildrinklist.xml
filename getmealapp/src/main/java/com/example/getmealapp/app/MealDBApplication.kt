package com.example.getmealapp.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Meal d b application.
 *
 * @constructor Create empty Meal d b application
 */
@HiltAndroidApp
class MealDBApplication : Application()
