package com.example.getmealapp.model.repository

import com.example.getmealapp.model.entity.CategoryMeal
import com.example.getmealapp.model.entity.MealByArea
import com.example.getmealapp.model.entity.MealByName
import com.example.getmealapp.model.entity.MealIngredient
import com.example.getmealapp.model.entity.SelectedCategoryMeal
import com.example.getmealapp.model.remote.ApiService
import com.example.getmealapp.repository.Repository
import com.example.getmealapp.utils.Resource
import com.example.getmealapp.utils.mapper.CategoryMealMapper
import com.example.getmealapp.utils.mapper.SelectedCategoryMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Repository impl.
 *
 * @property apiService
 * @constructor Create empty Repository impl
 */
class RepositoryImpl @Inject constructor(private val apiService: ApiService) : Repository {
    private val mapper = CategoryMealMapper()
    private val categoryMapper = SelectedCategoryMapper()

    override suspend fun getCategoryList(): Resource<List<CategoryMeal>> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val res = apiService.getAllCategoryMeal()
                if (res.isSuccessful && res.body() != null) {
                    Resource.Success(res.body()!!.categories.map { mapper(it) })
                } else {
                    Resource.Error(exception = "Error Retrieving data from API")
                }
            } catch (e: IllegalArgumentException) {
                Resource.Error(e.message.toString())
            }
        }

    override suspend fun getSelectedCategoryList(category: String): Resource<List<SelectedCategoryMeal>> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val res = apiService.getSelectedCategory(category)
                if (res.isSuccessful && res.body() != null) {
                    Resource.Success(
                        res.body()!!.meals.map {
                            categoryMapper(it)
                        }
                    )
                } else {
                    Resource.Error(exception = "Error Retrieving Selected Category")
                }
            } catch (e: IllegalArgumentException) {
                Resource.Error(e.message.toString())
            }
        }

    override suspend fun getMealByName(search: String): Resource<List<MealByName>> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val res = apiService.getMealByName(search)
                println("RESPONSE IS $res")

                if (res.isSuccessful && res.body() != null) {
                    res.body()?.let {
                        Resource.Success(it.meals ?: emptyList())
                    } ?: Resource.Error("IDK wtf happened.")
                } else {
                    Resource.Error(exception = "Error Retrieving Specific Meal")
                }
            } catch (e: IllegalArgumentException) {
                Resource.Error(e.message.toString())
            }
        }

    override suspend fun getAreaList(area: String): Resource<List<MealByArea>> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val res = apiService.getAllAreaMeal("list")
                if (res.isSuccessful && res.body() != null) {
                    Resource.Success(res.body()!!.meals.map { MealByArea(strArea = it.strArea) })
                } else {
                    Resource.Error("Error retrieivng data")
                }
            } catch (e: IllegalArgumentException) {
                Resource.Error(e.message.toString())
            }
        }

    override suspend fun getSelectedAreaMeal(area: String): Resource<List<SelectedCategoryMeal>> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val res = apiService.getSelectedArea(area)
                if (res.isSuccessful && res.body() != null) {
                    Resource.Success(res.body()!!.meals.map { categoryMapper(it) })
                } else {
                    Resource.Error("Error retrieving list")
                }
            } catch (e: IllegalArgumentException) {
                Resource.Error(e.message.toString())
            }
        }

    override suspend fun getIngredientList(ingredient: String): Resource<List<MealIngredient>> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val res = apiService.getAllIngredientMeal(ingredient)
                if (res.isSuccessful && res.body() != null) {
                    res.body()?.let {
                        Resource.Success(
                            res.body()!!.meals.map {
                                MealIngredient(
                                    strIngredient = it.strIngredient ?: "",
                                    strDescription = it.strDescription ?: "",
                                    strType = it.strType ?: "",
                                    idIngredient = it.idIngredient ?: ""
                                )
                            }
                        )
                    } ?: Resource.Error("IDK What HAPPENED")
                } else {
                    Resource.Error("Error retrieving data")
                }
            } catch (e: IllegalArgumentException) {
                Resource.Error(e.message.toString())
            }
        }

    override suspend fun getSelectedIngredient(ingredient: String): Resource<List<SelectedCategoryMeal>> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val response = apiService.getSelectedIngredient(ingredient)
                if (response.isSuccessful && response.body() != null) {
                    Resource.Success(response.body()!!.meals.map { categoryMapper(it) })
                } else {
                    Resource.Error("Error retrieivng selected ingredient list")
                }
            } catch (e: java.lang.IllegalArgumentException) {
                Resource.Error(e.message.toString())
            }
        }
}
