package com.example.getmealapp.model.entity

/**
 * Category meal  from API.
 *
 * @property idCategory
 * @property strCategory
 * @property strCategoryThumb
 * @property strCategoryDescription
 * @constructor Create empty Category meal
 */
data class CategoryMeal(
    val idCategory: String,
    val strCategory: String,
    val strCategoryThumb: String,
    val strCategoryDescription: String
)
