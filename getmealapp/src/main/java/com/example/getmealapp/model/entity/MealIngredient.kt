package com.example.getmealapp.model.entity

/**
 * Meal ingredient  from API.
 *
 * @property idIngredient
 * @property strDescription
 * @property strIngredient
 * @property strType
 * @constructor Create empty Meal ingredient
 */
data class MealIngredient(
    val idIngredient: String,
    val strDescription: String,
    val strIngredient: String,
    val strType: String? = ""
)
