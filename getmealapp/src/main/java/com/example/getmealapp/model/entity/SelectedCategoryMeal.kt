package com.example.getmealapp.model.entity

/**
 * Selected category meal.
 *
 * @property strMeal
 * @property strMealThumb
 * @property idMeal
 * @constructor Create empty Selected category meal
 */
data class SelectedCategoryMeal(
    val strMeal: String,
    val strMealThumb: String,
    val idMeal: String
)
