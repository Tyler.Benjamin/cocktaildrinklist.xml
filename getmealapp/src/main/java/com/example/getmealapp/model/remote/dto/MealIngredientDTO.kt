package com.example.getmealapp.model.remote.dto

/**
 * Meal ingredient d t o  from API.
 *
 * @property idIngredient
 * @property strDescription
 * @property strIngredient
 * @property strType
 * @constructor Create empty Meal ingredient d t o
 */
data class MealIngredientDTO(
    val idIngredient: String,
    val strDescription: String,
    val strIngredient: String,
    val strType: String? = ""
)
