package com.example.getmealapp.model.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.getmealapp.model.entity.MealData

/**
 * Meal database.
 *
 * @constructor Create empty Meal database
 */
@Database(entities = [MealData::class], version = 1)
abstract class MealDatabase : RoomDatabase() {
    abstract fun mealDao(): MealDao
}
