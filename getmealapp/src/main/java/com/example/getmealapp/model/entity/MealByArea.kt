package com.example.getmealapp.model.entity

/**
 * Meal by area Entity.
 *
 * @property strArea
 * @constructor Create empty Meal by area
 */
data class MealByArea(
    val strArea: String
)
