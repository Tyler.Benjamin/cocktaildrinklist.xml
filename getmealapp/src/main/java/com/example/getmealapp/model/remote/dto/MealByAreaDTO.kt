package com.example.getmealapp.model.remote.dto

/**
 * Meal by area DTO.
 *
 * @property strArea
 * @constructor Create empty Meal by area d t o
 */
data class MealByAreaDTO(
    val strArea: String
)
