package com.example.getmealapp.model.remote.dto

/**
 * Category meal d t o  from API.
 *
 * @property idCategory
 * @property strCategory
 * @property strCategoryThumb
 * @property strCategoryDescription
 * @constructor Create empty Category meal d t o
 */
data class CategoryMealDTO(
    val idCategory: String,
    val strCategory: String,
    val strCategoryThumb: String,
    val strCategoryDescription: String
)
