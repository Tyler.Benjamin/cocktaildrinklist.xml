package com.example.getmealapp.model.remote.dto

/**
 * Selected categroy response.
 *
 * @property meals
 * @constructor Create empty Selected categroy response
 */
data class SelectedCategroyResponse(
    val meals: List<SelectedCategoryMealDTO>
)
