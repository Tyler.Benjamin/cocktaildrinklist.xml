package com.example.getmealapp.model.remote.dto

/**
 * Data response.
 *
 * @property categories
 * @constructor Create empty Data response
 */
data class DataResponse(
    val categories: List<CategoryMealDTO>
)
