package com.example.getmealapp.model.remote.dto

/**
 * Ingredient response from API.
 *
 * @property meals
 * @constructor Create empty Ingredient response
 */
data class IngredientResponse(
    val meals: List<MealIngredientDTO>
)
