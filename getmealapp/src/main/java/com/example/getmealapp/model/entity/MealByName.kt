package com.example.getmealapp.model.entity

/**
 * Meal by name Entity.
 *
 * @property dateModified
 * @property idMeal
 * @property strArea
 * @property strCategory
 * @property strCreativeCommonsConfirmed
 * @property strDrinkAlternate
 * @property strImageSource
 * @property strIngredient1
 * @property strIngredient10
 * @property strIngredient11
 * @property strIngredient12
 * @property strIngredient13
 * @property strIngredient14
 * @property strIngredient15
 * @property strIngredient16
 * @property strIngredient17
 * @property strIngredient18
 * @property strIngredient19
 * @property strIngredient2
 * @property strIngredient20
 * @property strIngredient3
 * @property strIngredient4
 * @property strIngredient5
 * @property strIngredient6
 * @property strIngredient7
 * @property strIngredient8
 * @property strIngredient9
 * @property strInstructions
 * @property strMeal
 * @property strMealThumb
 * @property strMeasure1
 * @property strMeasure10
 * @property strMeasure11
 * @property strMeasure12
 * @property strMeasure13
 * @property strMeasure14
 * @property strMeasure15
 * @property strMeasure16
 * @property strMeasure17
 * @property strMeasure18
 * @property strMeasure19
 * @property strMeasure2
 * @property strMeasure20
 * @property strMeasure3
 * @property strMeasure4
 * @property strMeasure5
 * @property strMeasure6
 * @property strMeasure7
 * @property strMeasure8
 * @property strMeasure9
 * @property strSource
 * @property strTags
 * @property strYoutube
 * @constructor Create empty Meal by name
 */
data class MealByName(
    val dateModified: String? = "",
    val idMeal: String? = "",
    val strArea: String? = " ",
    val strCategory: String? = "",
    val strCreativeCommonsConfirmed: String? = "",
    val strDrinkAlternate: String? = "",
    val strImageSource: String? = "",
    val strIngredient1: String? = "",
    val strIngredient10: String? = "",
    val strIngredient11: String? = "",
    val strIngredient12: String? = "",
    val strIngredient13: String? = "",
    val strIngredient14: String? = "",
    val strIngredient15: String? = "",
    val strIngredient16: String? = "",
    val strIngredient17: String? = "",
    val strIngredient18: String? = "",
    val strIngredient19: String? = "",
    val strIngredient2: String? = "",
    val strIngredient20: String? = "",
    val strIngredient3: String? = "",
    val strIngredient4: String? = "",
    val strIngredient5: String? = "",
    val strIngredient6: String? = "",
    val strIngredient7: String? = "",
    val strIngredient8: String? = "",
    val strIngredient9: String? = "",
    val strInstructions: String? = "",
    val strMeal: String? = "",
    val strMealThumb: String? = "",
    val strMeasure1: String? = "",
    val strMeasure10: String? = "",
    val strMeasure11: String? = "",
    val strMeasure12: String? = "",
    val strMeasure13: String? = "",
    val strMeasure14: String? = "",
    val strMeasure15: String? = "",
    val strMeasure16: String? = "",
    val strMeasure17: String? = "",
    val strMeasure18: String? = "",
    val strMeasure19: String? = "",
    val strMeasure2: String? = "",
    val strMeasure20: String? = "",
    val strMeasure3: String? = "",
    val strMeasure4: String? = "",
    val strMeasure5: String? = "",
    val strMeasure6: String? = "",
    val strMeasure7: String? = "",
    val strMeasure8: String? = "",
    val strMeasure9: String? = "",
    val strSource: String? = "",
    val strTags: String? = "",
    val strYoutube: String? = ""
)
