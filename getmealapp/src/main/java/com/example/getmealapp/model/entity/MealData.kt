package com.example.getmealapp.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "meals")
data class MealData(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo
    val mealName: String
)
