package com.example.getmealapp.model.remote.dto

/**
 * Selected category meal d t o.
 *
 * @property strMeal
 * @property strMealThumb
 * @property idMeal
 * @constructor Create empty Selected category meal d t o
 */
data class SelectedCategoryMealDTO(
    val strMeal: String,
    val strMealThumb: String,
    val idMeal: String
)
