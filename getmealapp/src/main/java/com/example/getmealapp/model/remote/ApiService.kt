package com.example.getmealapp.model.remote

import com.example.getmealapp.model.remote.dto.AreaResponse
import com.example.getmealapp.model.remote.dto.DataResponse
import com.example.getmealapp.model.remote.dto.IngredientResponse
import com.example.getmealapp.model.remote.dto.MealByNameResponse
import com.example.getmealapp.model.remote.dto.SelectedCategroyResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Api service.
 *
 * @constructor Create empty Api service
 */
interface ApiService {

    @GET(CATEGORY_ENDPOINT)
    suspend fun getAllCategoryMeal(): Response<DataResponse>

    @GET(SELECTED_CATEGORY)
    suspend fun getSelectedCategory(@Query("c") category: String): Response<SelectedCategroyResponse>

    @GET(AREA_ENDPOINT)
    suspend fun getAllAreaMeal(@Query("a") area: String = "list"): Response<AreaResponse>

    @GET(SELECTED_AREA)
    suspend fun getSelectedArea(@Query("a") area: String): Response<SelectedCategroyResponse>

    @GET(INGREDIENT_ENDPOINT)
    suspend fun getAllIngredientMeal(@Query("i") ingredient: String = "list"): Response<IngredientResponse>

    @GET(SELECTED_CATEGORY)
    suspend fun getSelectedIngredient(@Query("i") ingredient: String): Response<SelectedCategroyResponse>

    @GET(MEAL_BY_NAME_ENDPOINT)
    suspend fun getMealByName(@Query("i") id: String): Response<MealByNameResponse>

    /**
     * Companion object for API SERVICE.
     *
     * @constructor Create empty Companion
     */
    companion object {
        const val CATEGORY_ENDPOINT = "categories.php"
        const val INGREDIENT_ENDPOINT = "list.php"
        const val AREA_ENDPOINT = "list.php"
        const val SELECTED_CATEGORY = "filter.php"
        const val MEAL_BY_NAME_ENDPOINT = "lookup.php"
        const val SELECTED_AREA = "filter.php"
    }
}
