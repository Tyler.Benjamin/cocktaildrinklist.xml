package com.example.getmealapp.model.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.getmealapp.model.entity.MealData

/**
 * Meal dao for Database access.
 *
 * @constructor Create empty Meal dao
 */
@Dao
interface MealDao {
    @Query("SELECT * FROM meals")
    fun getMeals(): List<MealData>

    @Query("SELECT * FROM meals WHERE id in (:id)")
    fun geMealById(id: Int): MealData

    @Insert(onConflict = REPLACE)
    fun insertMeal(meal: MealData)

    @Delete
    fun deleteMeal(meal: MealData)
}
