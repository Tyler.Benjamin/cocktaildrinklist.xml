package com.example.getmealapp.model.remote.dto

import com.example.getmealapp.model.entity.MealByName

@kotlinx.serialization.Serializable
data class MealByNameResponse(
    val meals: List<MealByName>?
)
