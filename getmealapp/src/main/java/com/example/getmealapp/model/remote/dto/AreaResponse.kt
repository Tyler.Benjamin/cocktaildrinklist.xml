package com.example.getmealapp.model.remote.dto

/**
 * Area response from API.
 *
 * @property meals
 * @constructor Create empty Area response
 */
data class AreaResponse(
    val meals: List<MealByAreaDTO>
)
