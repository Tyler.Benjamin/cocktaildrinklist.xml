package com.example.getmealapp.repository

import com.example.getmealapp.model.entity.CategoryMeal
import com.example.getmealapp.model.entity.MealByArea
import com.example.getmealapp.model.entity.MealByName
import com.example.getmealapp.model.entity.MealIngredient
import com.example.getmealapp.model.entity.SelectedCategoryMeal
import com.example.getmealapp.utils.Resource

/**
 * Repository.
 *
 * @constructor Create empty Repository
 */
interface Repository {
    /**
     * Get category list.
     *
     * @return
     */
    suspend fun getCategoryList(): Resource<List<CategoryMeal>>

    /**
     * Get selected category list.
     *
     * @param category
     * @return
     */
    suspend fun getSelectedCategoryList(category: String): Resource<List<SelectedCategoryMeal>>

    /**
     * Get meal by name.
     *
     * @param searchID
     * @return
     */
    suspend fun getMealByName(searchID: String): Resource<List<MealByName>>

    /**
     * Get area list.
     *
     * @param area
     * @return
     */
    suspend fun getAreaList(area: String = "list"): Resource<List<MealByArea>>

    /**
     * Get selected area meal.
     *
     * @param area
     * @return
     */
    suspend fun getSelectedAreaMeal(area: String): Resource<List<SelectedCategoryMeal>>

    /**
     * Get ingredient list.
     *
     * @param ingredient
     * @return
     */
    suspend fun getIngredientList(ingredient: String = "list"): Resource<List<MealIngredient>>

    /**
     * Get selected ingredient.
     *
     * @param ingredient
     * @return
     */
    suspend fun getSelectedIngredient(ingredient: String): Resource<List<SelectedCategoryMeal>>
}
