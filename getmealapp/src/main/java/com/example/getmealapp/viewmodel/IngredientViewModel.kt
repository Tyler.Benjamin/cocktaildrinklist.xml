package com.example.getmealapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.getmealapp.model.entity.MealIngredient
import com.example.getmealapp.model.repository.RepositoryImpl
import com.example.getmealapp.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Ingredient view model.
 *
 * @property repo
 * @constructor Create empty Ingredient view model
 */
@HiltViewModel
class IngredientViewModel @Inject constructor(private val repo: RepositoryImpl) : ViewModel() {

    private var _ingredientList: MutableLiveData<Resource<List<MealIngredient>>> = MutableLiveData()
    val ingredientList: LiveData<Resource<List<MealIngredient>>> = _ingredientList

    init {
        getIngredientList()
    }

    /**
     * Get ingredient list.
     *
     * @param ingredient
     */
    fun getIngredientList(ingredient: String = "list") {
        viewModelScope.launch {
            _ingredientList.value = repo.getIngredientList(ingredient)
        }
    }
}
