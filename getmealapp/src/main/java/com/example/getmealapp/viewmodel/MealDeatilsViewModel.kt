package com.example.getmealapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.getmealapp.model.entity.MealByName
import com.example.getmealapp.model.repository.RepositoryImpl
import com.example.getmealapp.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Meal deatils view model.
 *
 * @property repo
 * @constructor Create empty Meal deatils view model
 */
@HiltViewModel
class MealDeatilsViewModel @Inject constructor(private val repo: RepositoryImpl) : ViewModel() {
    private var _mealByNameList: MutableLiveData<Resource<List<MealByName>>> = MutableLiveData()
    val mealByNameList: LiveData<Resource<List<MealByName>>> = _mealByNameList

    /**
     * Get meal by name.
     *
     * @param search
     */
    fun getMealByName(search: String) {
        viewModelScope.launch {
            _mealByNameList.value = repo.getMealByName(search)
        }
    }
}
