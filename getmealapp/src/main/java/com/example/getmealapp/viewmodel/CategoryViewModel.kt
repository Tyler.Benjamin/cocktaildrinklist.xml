package com.example.getmealapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.getmealapp.model.entity.CategoryMeal
import com.example.getmealapp.model.repository.RepositoryImpl
import com.example.getmealapp.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Category view model.
 *
 * @property repo
 * @constructor Create empty Category view model
 */
@HiltViewModel
class CategoryViewModel @Inject constructor(private val repo: RepositoryImpl) : ViewModel() {
    private var _categoryList: MutableLiveData<Resource<List<CategoryMeal>>> = MutableLiveData()
    val categoryList: LiveData<Resource<List<CategoryMeal>>> = _categoryList

    init {
        getCategoryList()
    }

    /**
     * Get category list.
     *
     */
    fun getCategoryList() {
        viewModelScope.launch {
            _categoryList.value = repo.getCategoryList()
        }
    }
}
