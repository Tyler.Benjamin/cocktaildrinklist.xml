package com.example.getmealapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.getmealapp.model.entity.MealByArea
import com.example.getmealapp.model.repository.RepositoryImpl
import com.example.getmealapp.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Area view model.
 *
 * @property repo
 * @constructor Create empty Area view model
 */
@HiltViewModel
class AreaViewModel @Inject constructor(private val repo: RepositoryImpl) : ViewModel() {
    private var _areaFilterList: MutableLiveData<Resource<List<MealByArea>>> = MutableLiveData()
    val areaFilterList: LiveData<Resource<List<MealByArea>>> = _areaFilterList

    init {
        getMealByArea()
    }

    /**
     * Get meal by area.
     *
     * @param area
     */
    fun getMealByArea(area: String = "list") {
        viewModelScope.launch {
            _areaFilterList.value = repo.getAreaList(area)
        }
    }
}
