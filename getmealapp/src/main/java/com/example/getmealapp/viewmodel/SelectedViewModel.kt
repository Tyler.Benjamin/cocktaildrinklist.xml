package com.example.getmealapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.getmealapp.model.entity.SelectedCategoryMeal
import com.example.getmealapp.model.repository.RepositoryImpl
import com.example.getmealapp.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Selected view model.
 *
 * @property repo
 * @constructor Create empty Selected view model
 */
@HiltViewModel
class SelectedViewModel @Inject constructor(val repo: RepositoryImpl) : ViewModel() {

    private var _selectedList: MutableLiveData<Resource<List<SelectedCategoryMeal>>> = MutableLiveData()
    val selectedList: LiveData<Resource<List<SelectedCategoryMeal>>> = _selectedList

    /**
     * Get selected category meals.
     *
     * @param selected
     */
    fun getSelectedCategoryMeals(selected: String) {
        viewModelScope.launch {
            _selectedList.value = repo.getSelectedCategoryList(selected)
        }
    }
}
