package com.example.getmealapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.getmealapp.model.entity.SelectedCategoryMeal
import com.example.getmealapp.model.repository.RepositoryImpl
import com.example.getmealapp.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Selected ingredient view model.
 *
 * @property repo
 * @constructor Create empty Selected ingredient view model
 */
@HiltViewModel
class SelectedIngredientViewModel @Inject constructor(private val repo: RepositoryImpl) : ViewModel() {
    private var _selectedList: MutableLiveData<Resource<List<SelectedCategoryMeal>>> = MutableLiveData()
    val selectedList: LiveData<Resource<List<SelectedCategoryMeal>>> = _selectedList

    /**
     * Get selected ingredient list.
     *
     * @param ingredient
     */
    fun getSelectedIngredientList(ingredient: String) {
        viewModelScope.launch {
            _selectedList.value = repo.getSelectedIngredient(ingredient)
        }
    }
}
