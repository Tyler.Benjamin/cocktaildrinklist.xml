package com.example.getmealapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.getmealapp.databinding.FragmentAreaBinding
import com.example.getmealapp.utils.Resource
import com.example.getmealapp.view.adapter.AreaAdapter
import com.example.getmealapp.viewmodel.AreaViewModel
import kotlinx.coroutines.launch

/**
 * Area fragment.
 *
 * @constructor Create empty Area fragment
 */
class AreaFragment : Fragment() {
    private var _binding: FragmentAreaBinding? = null
    val binding: FragmentAreaBinding get() = _binding!!
    private val areaViewModel by activityViewModels<AreaViewModel>()
    private val areaAdapter by lazy { AreaAdapter(::navigateToSelectedArea) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentAreaBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Init views.
     *
     */
    fun initViews() = with(binding) {
        lifecycleScope.launch {
            areaViewModel.areaFilterList.observe(
                viewLifecycleOwner,
                Observer { areaList ->
                    when (areaList) {
                        is Resource.Error -> TODO()
                        Resource.Loading -> TODO()
                        is Resource.Success ->
                            rvAreaFragment.adapter =
                                areaAdapter.apply { applyAreaList(areaList.data) }
                    }
                }
            )
        }
    }

    /**
     * Navigate to selected area.
     *
     * @param area
     */
    fun navigateToSelectedArea(area: String) {
        val action = AreaFragmentDirections.actionAreaFragmentToSelectedAreaFragment(area)
        findNavController().navigate(action)
    }
}
