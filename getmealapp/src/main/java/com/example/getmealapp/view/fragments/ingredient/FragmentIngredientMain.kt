package com.example.getmealapp.view.fragments.ingredient

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.getmealapp.R
import com.example.getmealapp.databinding.FragmentIngredientMainBinding
import com.example.getmealapp.utils.Resource
import com.example.getmealapp.view.adapter.IngredientAdapter
import com.example.getmealapp.viewmodel.IngredientViewModel
import kotlinx.coroutines.launch

/**
 * Fragment ingredient main.
 *
 * @constructor Create empty Fragment ingredient main
 */
class FragmentIngredientMain : Fragment() {
    private var _binding: FragmentIngredientMainBinding? = null
    val binding: FragmentIngredientMainBinding get() = _binding!!
    private val ingredientAdapter by lazy { IngredientAdapter(::navigateToSelctedIngredient) }
    private val viewModel by activityViewModels<IngredientViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentIngredientMainBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        binding.btnBack.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.btnHome.setOnClickListener {
            findNavController().navigate(R.id.dashboardFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Init views this automaticallyg makes call to repo when viewmodel initialized.
     *
     */
    fun initViews() = with(binding) {
        lifecycleScope.launch {
            viewModel.ingredientList.observe(
                viewLifecycleOwner,
                Observer { ingredientList ->
                    when (ingredientList) {
                        is Resource.Error -> Log.e("ERROR", "initViews: ERROR")
                        Resource.Loading -> Log.e("LOADING", "initViews: LOADING ERROR ")
                        is Resource.Success ->
                            rvIngredient.adapter =
                                ingredientAdapter.apply { addIngredientList(ingredientList.data) }
                    }
                }
            )
        }
    }

    /**
     * Navigate to selcted ingredient.
     *
     * @param ingredient
     */
    fun navigateToSelctedIngredient(ingredient: String) {
        val action = FragmentIngredientMainDirections
            .actionFragmentIngredientMainToSelectedIngredientFragment(ingredient)
        findNavController().navigate(action)
    }
}
