package com.example.getmealapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.getmealapp.R
import com.example.getmealapp.databinding.FragmentMealDetailsBinding
import com.example.getmealapp.model.entity.MealByName
import com.example.getmealapp.utils.Constants
import com.example.getmealapp.utils.Resource
import com.example.getmealapp.viewmodel.MealDeatilsViewModel
import kotlinx.coroutines.launch

/**
 * Meal deatails fragment.
 *
 * @constructor Create empty Meal deatails fragment
 */
class MealDeatailsFragment : Fragment() {

    private var _binding: FragmentMealDetailsBinding? = null
    val binding: FragmentMealDetailsBinding get() = _binding!!
    private val mealDeatilsViewModel by activityViewModels<MealDeatilsViewModel>()
    private val args: MealDeatailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentMealDetailsBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding) {
        super.onViewCreated(view, savedInstanceState)
        mealDeatilsViewModel.getMealByName(args.mealId)
        initViews()
        btnBack.setOnClickListener {
            findNavController().navigateUp()
        }

        btnHome.setOnClickListener {
            findNavController().navigate(R.id.dashboardFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Init views.
     *
     */
    fun initViews() {
        lifecycleScope.launch {
            mealDeatilsViewModel.mealByNameList.observe(
                viewLifecycleOwner,
                Observer { mealbyName ->
                    when (mealbyName) {
                        is Resource.Error -> TODO()
                        Resource.Loading -> TODO()
                        is Resource.Success ->
                            applyMealByName(mealbyName.data.get(0))
                    }
                }
            )
        }
    }

    /**
     * Apply meal by name for binding data to view.
     *
     * @param mealByName
     */
    fun applyMealByName(mealByName: MealByName) = with(binding) {
        binding.tvPagetitle.text = mealByName.strMeal
        binding.mealDescription.text = mealByName.strInstructions
        binding.ingredient1.text = mealByName.strIngredient1
        binding.ingredient2.text = mealByName.strIngredient2
        binding.ingredient3.text = mealByName.strIngredient3
        binding.ingredient4.text = mealByName.strIngredient4
        binding.imageViewSelected.load(Constants.MEAL_IMAGE + Constants.MEAL_IMAGE_TWO)
    }
}
