package com.example.getmealapp.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.getmealapp.databinding.ItemIngredientBinding
import com.example.getmealapp.model.entity.MealIngredient
import com.example.getmealapp.utils.Constants

/**
 * Ingredient adapter.
 *
 * @property navigate
 * @constructor Create empty Ingredient adapter
 */
class IngredientAdapter(
    val navigate: (ingredient: String) -> Unit
) : RecyclerView.Adapter<IngredientAdapter.IngredientViewHolder>() {
    private var ingredientList: List<MealIngredient> = mutableListOf()

    /**
     * Ingredient view holder.
     *
     * @property binding
     * @constructor Create empty Ingredient view holder
     */
    inner class IngredientViewHolder(
        val binding: ItemIngredientBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        /**
         * Apply ingredient list to view.
         *
         * @param ingredient
         */
        fun applyIngredientList(ingredient: MealIngredient) = with(binding) {
            tvCategoryTitle.text = ingredient.strIngredient
            tvCategoryDescription.text = ingredient.strDescription
            imageViewCategory.load(Constants.MEAL_IMAGE + Constants.MEAL_IMAGE_TWO)

            root.setOnClickListener {
                navigate(ingredient.strIngredient)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientViewHolder {
        val itemBinding =
            ItemIngredientBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return IngredientViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: IngredientViewHolder, position: Int) {
        val ingredient = ingredientList[position]
        holder.applyIngredientList(ingredient)
    }

    override fun getItemCount(): Int = ingredientList.size

    /**
     * Add ingredient list.
     *
     * @param list
     */
    fun addIngredientList(list: List<MealIngredient>) {
        this.ingredientList = list
    }
}
