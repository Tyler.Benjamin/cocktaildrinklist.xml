package com.example.getmealapp.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.getmealapp.databinding.ItemListBinding
import com.example.getmealapp.model.entity.CategoryMeal

/**
 * Meal adapter.
 *
 * @property navigate
 * @constructor Create empty Meal adapter
 */
class MealAdapter(
    val navigate: (category: String, description: String) -> Unit
) : RecyclerView.Adapter<MealAdapter.MealViewHolder>() {
    private var categoryList: List<CategoryMeal> = mutableListOf()

    /**
     * Meal view holder.
     *
     * @property binding
     * @constructor Create empty Meal view holder
     */
    inner class MealViewHolder(
        val binding: ItemListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        /**
         * Apply category drink.
         *
         * @param categoryMeal
         */
        fun applyCategoryDrink(categoryMeal: CategoryMeal) = with(binding) {
            tvCategoryTitle.text = categoryMeal.strCategory
            imageViewCategory.load(categoryMeal.strCategoryThumb)
            tvCategoryDescription.text = categoryMeal.strCategoryDescription

            root.setOnClickListener {
                navigate(categoryMeal.strCategory, categoryMeal.strCategoryDescription)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealViewHolder {
        val itemBinding =
            ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MealViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: MealViewHolder, position: Int) {
        val category = categoryList[position]
        holder.applyCategoryDrink(category)
    }

    override fun getItemCount(): Int = categoryList.size

    /**
     * Add category list.
     *
     * @param categoryList
     */
    fun addCategoryList(categoryList: List<CategoryMeal>) {
        this.categoryList = categoryList
    }
}
