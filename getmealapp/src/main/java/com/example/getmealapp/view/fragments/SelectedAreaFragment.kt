package com.example.getmealapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.getmealapp.R
import com.example.getmealapp.databinding.FragmentSelectedAreaBinding
import com.example.getmealapp.utils.Resource
import com.example.getmealapp.view.adapter.SelectedAdapter
import com.example.getmealapp.viewmodel.SelectedAreaViewModel
import kotlinx.coroutines.launch

/**
 * Selected area fragment.
 *
 * @constructor Create empty Selected area fragment
 */
class SelectedAreaFragment : Fragment() {
    private var _binding: FragmentSelectedAreaBinding? = null
    val binding: FragmentSelectedAreaBinding get() = _binding!!
    private val selectedAreaVM by activityViewModels<SelectedAreaViewModel>()
    private val args: SelectedAreaFragmentArgs by navArgs()
    private val selectedAdapter by lazy { SelectedAdapter(::navigateToSpecificMeal) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentSelectedAreaBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        binding.textViewPageTitle.text = args.area
        binding.btnHome.setOnClickListener {
            findNavController().navigate(R.id.dashboardFragment)
        }
        binding.btnBack.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Init views.
     *
     */
    fun initViews() = with(binding) {
        lifecycleScope.launch {
            selectedAreaVM.getSelectedAreaMeals(args.area)
            selectedAreaVM.selectedList.observe(
                viewLifecycleOwner,
                Observer { selectedList ->
                    when (selectedList) {
                        is Resource.Error -> TODO()
                        Resource.Loading -> TODO()
                        is Resource.Success ->
                            rvSelected.adapter =
                                selectedAdapter.apply { addSelectedMealList(selectedList.data) }
                    }
                }
            )
        }
    }

    /**
     * Navigate to specific meal.
     *
     * @param id
     */
    fun navigateToSpecificMeal(id: String) {
        val action =
            SelectedAreaFragmentDirections.actionSelectedAreaFragmentToMealDeatailsFragment(id)
        findNavController().navigate(action)
    }
}
