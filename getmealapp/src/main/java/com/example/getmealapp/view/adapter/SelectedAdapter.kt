package com.example.getmealapp.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.getmealapp.databinding.SelectedListBinding
import com.example.getmealapp.model.entity.SelectedCategoryMeal

/**
 * Selected adapter for recycler view in Selected Area and Ingredient Fragment.
 *
 * @property navigate
 * @constructor Create empty Selected adapter
 */
class SelectedAdapter(
    val navigate: (id: String) -> Unit
) : RecyclerView.Adapter<SelectedAdapter.SelectedViewHolder>() {
    private var selectedMealList: List<SelectedCategoryMeal> = mutableListOf()

    /**
     * Selected view holder.
     *
     * @property binding
     * @constructor Create empty Selected view holder
     */
    inner class SelectedViewHolder(
        val binding: SelectedListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        /**
         * Apply selected meals.
         *
         * @param selectedMeal
         */
        fun applySelectedMeals(selectedMeal: SelectedCategoryMeal) = with(binding) {
            textViewMeal.text = selectedMeal.strMeal
            imageViewSelected.load(selectedMeal.strMealThumb)

            root.setOnClickListener {
                navigate(selectedMeal.idMeal)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectedViewHolder {
        val itemBinding = SelectedListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SelectedViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: SelectedViewHolder, position: Int) {
        val meal = selectedMealList[position]
        holder.applySelectedMeals(meal)
    }

    override fun getItemCount(): Int = selectedMealList.size

    /**
     * Add selected meal list.
     *
     * @param selectedMeals
     */
    fun addSelectedMealList(selectedMeals: List<SelectedCategoryMeal>) {
        this.selectedMealList = selectedMeals
    }
}
