package com.example.getmealapp.view.fragments.ingredient

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.getmealapp.R
import com.example.getmealapp.databinding.FragmentSelectedIngredientBinding
import com.example.getmealapp.utils.Resource
import com.example.getmealapp.view.adapter.SelectedAdapter
import com.example.getmealapp.viewmodel.SelectedIngredientViewModel
import kotlinx.coroutines.launch

/**
 * Selected ingredient fragment.
 *
 * @constructor Create empty Selected ingredient fragment
 */
class SelectedIngredientFragment : Fragment() {
    private var _binding: FragmentSelectedIngredientBinding? = null
    val binding: FragmentSelectedIngredientBinding get() = _binding!!
    private val selectedIngredientVM by activityViewModels<SelectedIngredientViewModel>()
    private val args: SelectedIngredientFragmentArgs by navArgs()
    private val selectedAdapter by lazy { SelectedAdapter(::navigateToSpecificMeal) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentSelectedIngredientBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        binding.textViewPageTitle.text = args.ingredient
        binding.btnHome.setOnClickListener {
            findNavController().navigate(R.id.dashboardFragment)
        }
        binding.btnBack.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Init views for calling to repo when viewmodel is initialized.
     *
     */
    fun initViews() = with(binding) {
        lifecycleScope.launch {
            selectedIngredientVM.getSelectedIngredientList(args.ingredient)
            selectedIngredientVM.selectedList.observe(
                viewLifecycleOwner,
                Observer { selectedList ->
                    when (selectedList) {
                        is Resource.Error -> TODO()
                        Resource.Loading -> TODO()
                        is Resource.Success ->
                            rvSelected.adapter =
                                selectedAdapter.apply { addSelectedMealList(selectedList.data) }
                    }
                }
            )
        }
    }

    /**
     * Navigate to specific meal.
     *
     * @param id
     */
    fun navigateToSpecificMeal(id: String) {
        val action =
            SelectedIngredientFragmentDirections.actionSelectedIngredientFragmentToMealDeatailsFragment(
                id
            )
        findNavController().navigate(action)
    }
}
