package com.example.getmealapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.getmealapp.R
import com.example.getmealapp.databinding.FragmentDashboardBinding

/**
 * Dashboard fragment.
 *
 * @constructor Create empty Dashboard fragment
 */
class DashboardFragment : Fragment() {
    private var _binding: FragmentDashboardBinding? = null
    val binding: FragmentDashboardBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentDashboardBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Init views.
     *
     */
    fun initViews() = with(binding) {
        btnCategories.setOnClickListener {
            findNavController().navigate(R.id.categoryFragment)
        }
        btnArea.setOnClickListener {
            println("Area button clicked")
            findNavController().navigate(R.id.areaFragment)
        }
        btnSaved.setOnClickListener {
            println("Saved button clicked")
        }
        btnIngredient.setOnClickListener {
            println("ingredient button clicked")
            findNavController().navigate(R.id.fragmentIngredientMain)
        }
    }
}
