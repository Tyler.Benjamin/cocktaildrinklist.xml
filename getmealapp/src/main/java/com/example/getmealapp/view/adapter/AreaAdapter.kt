package com.example.getmealapp.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.getmealapp.databinding.ItemAreaBinding
import com.example.getmealapp.model.entity.MealByArea
import com.example.getmealapp.utils.Constants

/**
 * Area adapter.
 *
 * @property navigate
 * @constructor Create empty Area adapter
 */
class AreaAdapter(
    val navigate: (area: String) -> Unit
) : RecyclerView.Adapter<AreaAdapter.AreaViewHolder>() {
    private var areaList: List<MealByArea> = mutableListOf()

    /**
     * Area view holder.
     *
     * @property binding
     * @constructor Create empty Area view holder
     */
    inner class AreaViewHolder(
        val binding: ItemAreaBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        /**
         * Apply area list to view.
         *
         * @param area
         */
        fun applyAreaList(area: MealByArea) = with(binding) {
            tvMeal.text = area.strArea
            imageViewMeal.load(Constants.MEAL_IMAGE + Constants.MEAL_IMAGE_TWO)

            root.setOnClickListener {
                navigate(area.strArea)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AreaViewHolder {
        val itemBinding =
            ItemAreaBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AreaViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: AreaViewHolder, position: Int) {
        val area = areaList[position]
        holder.applyAreaList(area)
    }

    override fun getItemCount(): Int = areaList.size

    /**
     * Apply area list.
     *
     * @param areaList
     */
    fun applyAreaList(areaList: List<MealByArea>) {
        this.areaList = areaList
    }
}
