package com.example.getmealapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.getmealapp.R
import com.example.getmealapp.databinding.FragmentCategoryBinding
import com.example.getmealapp.utils.Resource
import com.example.getmealapp.view.adapter.MealAdapter
import com.example.getmealapp.viewmodel.CategoryViewModel
import kotlinx.coroutines.launch

/**
 * Category fragment.
 *
 * @constructor Create empty Category fragment
 */
class CategoryFragment : Fragment() {
    private var _binding: FragmentCategoryBinding? = null
    val binding: FragmentCategoryBinding get() = _binding!!
    private val categoryVM by activityViewModels<CategoryViewModel>()
    private val categoryAdapter by lazy { MealAdapter(::navigateToSelected) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentCategoryBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        binding.btnBack.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.btnHome.setOnClickListener {
            findNavController().navigate(R.id.dashboardFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Init views.
     *
     */
    fun initViews() = with(binding) {
        lifecycleScope.launch {
            categoryVM.categoryList.observe(
                viewLifecycleOwner,
                Observer { categoryMeals ->
                    when (categoryMeals) {
                        is Resource.Error -> TODO()
                        Resource.Loading -> TODO()
                        is Resource.Success ->
                            rvIngredient.adapter =
                                categoryAdapter.apply { addCategoryList(categoryMeals.data) }
                    }
                }
            )
        }
    }

    /**
     * Navigate to selected.
     *
     * @param category
     * @param description
     */
    fun navigateToSelected(
        category: String,
        description: String
    ) {
        val action = CategoryFragmentDirections.actionCategoryFragmentToSelectedCategoryFragment(
            category,
            description
        )
        findNavController().navigate(action)
    }
}
