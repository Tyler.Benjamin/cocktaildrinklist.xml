package com.example.getmealapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.getmealapp.R
import com.example.getmealapp.databinding.FragmentSelectedCategoryBinding
import com.example.getmealapp.utils.Resource
import com.example.getmealapp.view.adapter.SelectedAdapter
import com.example.getmealapp.viewmodel.SelectedViewModel
import kotlinx.coroutines.launch

/**
 * Selected category fragment.
 *
 * @constructor Create empty Selected category fragment
 */
class SelectedCategoryFragment : Fragment() {
    private var _binding: FragmentSelectedCategoryBinding? = null
    val binding: FragmentSelectedCategoryBinding get() = _binding!!
    private val selectedViewModel by activityViewModels<SelectedViewModel>()
    private val args: SelectedCategoryFragmentArgs by navArgs()
    private val selectedAdapter by lazy { SelectedAdapter(::navigateToMealByName) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentSelectedCategoryBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        binding.textViewPageTitle.text = args.category
        binding.textViewPageBody.text = args.description
        binding.btnHome.setOnClickListener {
            findNavController().navigate(R.id.dashboardFragment)
        }
        binding.btnBack.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Init views.
     *
     */
    fun initViews() = with(binding) {
        lifecycleScope.launch {
            selectedViewModel.getSelectedCategoryMeals(args.category)
            selectedViewModel.selectedList.observe(
                viewLifecycleOwner,
                Observer { selectedList ->
                    when (selectedList) {
                        is Resource.Error -> TODO()
                        Resource.Loading -> TODO()
                        is Resource.Success ->
                            rvSelected.adapter =
                                selectedAdapter.apply { addSelectedMealList(selectedList.data) }
                    }
                }
            )
        }
    }

    /**
     * Navigate to meal by name.
     *
     * @param id
     */
    fun navigateToMealByName(id: String) {
        val action =
            SelectedCategoryFragmentDirections.actionSelectedCategoryFragmentToMealDeatailsFragment(
                id
            )
        findNavController().navigate(action)
    }
}
