package com.example.getmealapp.utils.mapper

import com.example.getmealapp.model.entity.MealByArea
import com.example.getmealapp.model.remote.dto.MealByAreaDTO

/**
 * Meal by area mapper.
 *
 * @constructor Create empty Meal by area mapper
 */
class MealByAreaMapper : Mapper<MealByAreaDTO, MealByArea> {
    override fun invoke(dto: MealByAreaDTO): MealByArea {
        return with(dto) {
            MealByArea(strArea)
        }
    }
}
