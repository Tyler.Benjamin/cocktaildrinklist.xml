package com.example.getmealapp.utils.mapper

import com.example.getmealapp.model.entity.CategoryMeal
import com.example.getmealapp.model.remote.dto.CategoryMealDTO

/**
 * Category meal mapper.
 *
 * @constructor Create empty Category meal mapper
 */
class CategoryMealMapper : Mapper<CategoryMealDTO, CategoryMeal> {
    override fun invoke(dto: CategoryMealDTO): CategoryMeal {
        return with(dto) {
            CategoryMeal(
                strCategory = strCategory,
                strCategoryThumb = strCategoryThumb,
                strCategoryDescription = strCategoryDescription,
                idCategory = idCategory
            )
        }
    }
}
