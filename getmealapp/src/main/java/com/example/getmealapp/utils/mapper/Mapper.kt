package com.example.getmealapp.utils.mapper

/**
 * Mapper.
 *
 * @param DTO
 * @param ENTITY
 * @constructor Create empty Mapper
 */
interface Mapper<in DTO, out ENTITY> {
    /**
     * Invoke function for mapping.
     *
     * @param dto
     * @return
     */
    operator fun invoke(dto: DTO): ENTITY
}
