package com.example.getmealapp.utils.mapper

import com.example.getmealapp.model.entity.SelectedCategoryMeal
import com.example.getmealapp.model.remote.dto.SelectedCategoryMealDTO

/**
 * Selected category mapper.
 *
 * @constructor Create empty Selected category mapper
 */
class SelectedCategoryMapper : Mapper<SelectedCategoryMealDTO, SelectedCategoryMeal> {
    override fun invoke(dto: SelectedCategoryMealDTO): SelectedCategoryMeal {
        return with(dto) {
            SelectedCategoryMeal(
                strMeal = strMeal,
                strMealThumb = strMealThumb,
                idMeal = idMeal
            )
        }
    }
}
