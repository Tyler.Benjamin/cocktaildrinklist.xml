package com.example.getmealapp.utils

/**
 * Constants.
 *
 * @constructor Create empty Constants
 */
@Suppress("indent")
object Constants {
    const val BASE_URL = "https://www.themealdb.com/api/json/v1/1/"
    const val MEAL_IMAGE =
        "https://images.unsplash.com/photo-1546069901-ba9599a7e63c?ixlib=rb-4.0"
    const val MEAL_IMAGE_TWO =
        ".3&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&w=1000&q=80"
}
