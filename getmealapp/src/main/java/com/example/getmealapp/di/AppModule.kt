package com.example.getmealapp.di

import android.content.Context
import androidx.room.Room
import com.example.getmealapp.model.local.MealDao
import com.example.getmealapp.model.local.MealDatabase
import com.example.getmealapp.model.remote.ApiService
import com.example.getmealapp.model.repository.RepositoryImpl
import com.example.getmealapp.repository.Repository
import com.example.getmealapp.utils.Constants.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun providesRoomDB(@ApplicationContext applicationContext: Context): MealDao = Room.databaseBuilder(
        applicationContext,
        MealDatabase::class.java,
        "phonebook-database"
    ).build().mealDao()

    @Provides
    @Singleton
    fun providesRetrofit(): ApiService {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    @Provides
    @Singleton
    fun providesRepositoryImpl(apiService: ApiService): Repository = RepositoryImpl(apiService)
}
